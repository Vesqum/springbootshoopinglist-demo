package it.devnotes.demo.service;

import it.devnotes.demo.model.CartItem;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

@Scope(
    value = WebApplicationContext.SCOPE_SESSION,
    proxyMode = ScopedProxyMode.TARGET_CLASS
)
@Service
public class ShoppingListServiceMemoryImpl implements ShoppingListService {

    private List<CartItem> cartItems = new ArrayList<>();

    @Override
    public void addItem(CartItem item) {
        cartItems.add(item);
    }

    @Override
    public void deleteItem(CartItem item) {
        cartItems.remove(item);
    }

    @Override
    public void clearList() {
        cartItems = new ArrayList<>();
    }

    @Override
    public List<CartItem> getAllShoppingListItems() {
        return cartItems;
    }
}
